variable "env" {}
variable "folder_id" {}
variable "network_id" {}
variable "service_account_id" {}
variable "zone" {}
variable "yandex_vpc_subnet_id" {}
variable "release_chan" {
  default = "REGULAR"
}
variable "network_provider" {
  default = "CALICO"
}
variable "ver" {
  default = "1.19"
}
