output "yandex_vpc_network" {
  value = yandex_vpc_network.internal.id
}

output "yandex_vpc_subnet_id" {
  value = yandex_vpc_subnet.subnet.id
}
