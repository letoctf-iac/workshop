terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.77.0"
    }
  }
}

resource "yandex_kubernetes_node_group" "group" {
  cluster_id  = var.kubernetes_cluster_id
  name        = var.group_name
  version     = var.ver
  node_labels = var.labels
  node_taints = var.taints
  instance_template {
    platform_id = var.platform
    network_interface {
      nat                = true
      subnet_ids         = [var.yandex_vpc_subnet]
      security_group_ids = var.security_groupd
    }
    resources {
      core_fraction = var.core
      memory        = var.memory
      cores         = var.cpu
    }
    boot_disk {
      type = var.disk_type
      size = var.disk_size
    }
    scheduling_policy {
      preemptible = false
    }
  }
  scale_policy {
    auto_scale {
      initial = var.node_init
      max     = var.node_max
      min     = var.node_min
    }
  }
  allocation_policy {
    location {
      zone = var.zone
    }
  }
  maintenance_policy {
    auto_repair  = true
    auto_upgrade = false
  }
}