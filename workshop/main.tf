module "vpc" {
  source    = "../modules/vpc"
  cidr      = ["10.202.0.0/16"]
  env       = var.env
  zone      = var.zone
  folder_id = var.folder_id
}

module "sa" {
  source    = "../modules/sa"
  env       = var.env
  folder_id = var.folder_id
}

module "kubernetes" {
  source               = "../modules/kubernetes"
  env                  = var.env
  zone                 = var.zone
  folder_id            = var.folder_id
  network_id           = module.vpc.yandex_vpc_network
  service_account_id   = module.sa.service_account_id
  yandex_vpc_subnet_id = module.vpc.yandex_vpc_subnet_id
}

module "nodes_workers" {
  source                = "../modules/kubernetes_node_group"
  kubernetes_cluster_id = module.kubernetes.kubernetes_cluster_id
  zone                  = var.zone
  group_name            = "workers"
  yandex_vpc_subnet     = module.vpc.yandex_vpc_subnet_id
}

module "nodes_ingress" {
  source                = "../modules/kubernetes_node_group"
  kubernetes_cluster_id = module.kubernetes.kubernetes_cluster_id
  zone                  = var.zone
  group_name            = "ingress"
  yandex_vpc_subnet     = module.vpc.yandex_vpc_subnet_id
  taints                = ["node.role/ingress=:NoSchedule"]
  labels = {
    "node.role/ingress" = "true"
  }
}