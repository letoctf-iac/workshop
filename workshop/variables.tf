variable "folder_id" {
  description = "Yandex Cloud Folder ID where resources will be created"
  default     = ""
}

variable "cloud_id" {
  description = "Yandex Cloud ID where resources will be created"
  default     = ""
}

variable "zone" {
  description = "Yandex Cloud default Zone for provisoned resources"
  default     = "ru-central1-a"
}

variable "env" {
  default = "workshop"
}