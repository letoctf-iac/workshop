# Подготовка репо к работе
## Создаем в YC фолдер для работы
```shell
yc resource-manager folder create --name default

```

## Create serviceaccount
```shell
yc iam service-account create tfbot-sa-default
yc iam key create --service-account-name tfbot-sa-default --output key.json 

yc resource-manager folder add-access-binding --name default --role admin --service-account-name tfbot-sa-default
```


## Прописываем ID ресурсов и ключи в .gitlab-ci.yml
```yaml
variables:
 TF_STATE_NAME: default
 TF_CACHE_KEY: default
 TF_ROOT: stage

 TF_VAR_cloud_id: b1gsokh8d3t9uqa7qlaq
 TF_VAR_folder_id: b1gb9g9po5r4juiuic2e
 YC_SERVICE_ACCOUNT_KEY_FILE:

```

Как получить?
```shell
# cloud_id
yc resource-manager cloud get --name noob4ik
yc resource-manager folder get --name default
cat ../key.json| jq -c
```


# Памятка по шагам:

## Шаг 1 - 
- создаем репку в Gitlab
- в репку добавляем gitlab-ci
- создаем папку как указано в TF_ROOT
- в папке создаем файл с провайдером
- пушим в ветку - делаем MR - наблюдаем пайплайн